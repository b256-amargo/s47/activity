
const textFirstName = document.querySelector("#txt-first-name");
const textLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

function fullName(){
	spanFullName.innerHTML = `${textFirstName.value} ${textLastName.value}`;
};
textFirstName.addEventListener("keyup", fullName);
textLastName.addEventListener("keyup", fullName);
